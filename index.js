require('dotenv').config();

const express = require('express');
const { dbConnection } = require('./database/config');
const cors = require('cors');


//Crear el servidor de express
const app = express();

//Base de datos
dbConnection();

app.use( cors() );


//Rutas
app.get('/',(req,res) =>{
    res.json({
        ok:true,
        msg:'hola mundo'
    })
})


app.listen( process.env.PORT , () =>{
    console.log("estamos en el puerto "+ process.env.PORT);
});